FROM ubuntu:16.04
 
#
# Identify the maintainer of an image
LABEL maintainer="leanghy@dynamo-tech.com"
 
#
# Update the image to the latest packages
RUN apt-get update && apt-get upgrade -y
 
#
# Install NGINX to test.
RUN apt-get install -y nginx
#Unlink default site to make sure the web server not conflig and give permission to directory /var/www/html  
RUN unlink /etc/nginx/sites-enabled/default \ 
    && chown www-data:www-data /var/www/html \ 
    && chmod -R 755 /var/www/html 

#
# Expose port 80
EXPOSE 80
 
#
# Last is the actual command to start up NGINX within our Container
CMD ["nginx", "-g", "daemon off;"]
